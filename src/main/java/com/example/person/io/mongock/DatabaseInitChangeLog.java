package com.example.person.io.mongock;

import com.example.person.collection.PersonCollection;
import com.example.person.repository.PersonRepository;
import com.github.cloudyrock.mongock.ChangeLog;
import io.mongock.api.annotations.ChangeUnit;
import io.mongock.api.annotations.Execution;
import io.mongock.api.annotations.RollbackExecution;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.Document;

@ChangeUnit(id="person-initializer", order = "1", author = "mongock")
public class DatabaseInitChangeLog {
    private final MongoTemplate mongoTemplate;
    private final PersonRepository personRepository;

    public DatabaseInitChangeLog(MongoTemplate mongoTemplate,
                                 PersonRepository personRepository) {
        this.mongoTemplate = mongoTemplate;
        this.personRepository = personRepository;
    }

    /** This is the method with the migration code **/
    @Execution
    public void changeSet() {
       personRepository.save(PersonCollection.builder().email("admin@mail.ru")
                                                        .name("ADMIN")
                                                        .phone("666-666")
                                                        .build());
    }

    /**
     This method is mandatory even when transactions are enabled.
     They are used in the undo operation and any other scenario where transactions are not an option.
     However, note that when transactions are avialble and Mongock need to rollback, this method is ignored.
     **/
    @RollbackExecution
    public void rollback() {

    }
}

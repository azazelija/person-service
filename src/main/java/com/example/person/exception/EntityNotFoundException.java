package com.example.person.exception;

public class EntityNotFoundException extends RuntimeException {

    public static String NOT_FOUND_UNDER_ID = "Can not find any person under given ID";

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

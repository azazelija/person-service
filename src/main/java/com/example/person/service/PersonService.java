package com.example.person.service;

import com.example.person.collection.PersonCollection;
import com.example.person.dto.Person;
import com.example.person.exception.EntityNotFoundException;
import com.example.person.mapper.PersonMapper;
import com.example.person.repository.PersonRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.sql.SQLOutput;
import java.util.Optional;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PersonMapper personMapper;

    public Person getById(String id) {
        Optional<PersonCollection> personCollection = personRepository.findById(id);
        if (personCollection.isPresent()) {
            return personMapper.collectionToDto(personCollection.get());
        }
        throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
    }

    public Person create(Person person) {
        PersonCollection personCollection = personMapper.dtoToCollection(person);
        personCollection = personRepository.save(personCollection);

        person.setId(personCollection.getId());
        return person;
    }

    public Person update(Person person) {
        Optional<PersonCollection> personCollectionOptional = personRepository.findById(person.getId());
        if (!personCollectionOptional.isPresent()) {
            throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
        }
        PersonCollection personCollection = personMapper.dtoToCollection(person);
        personRepository.save(personCollection);

        return person;
    }

    public void deleteById(String id) {
        personRepository.deleteById(id);
    }
}

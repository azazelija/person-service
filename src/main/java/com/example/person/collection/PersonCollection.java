package com.example.person.collection;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "person")
public class PersonCollection {

    @Id
    ObjectId id;

    String name;

    String email;

    String phone;
}

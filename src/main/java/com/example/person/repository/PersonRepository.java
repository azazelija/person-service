package com.example.person.repository;

import com.example.person.collection.PersonCollection;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.Optional;

public interface PersonRepository extends MongoRepository<PersonCollection, ObjectId> {

    Optional<PersonCollection> findById(String id);

    PersonCollection save(PersonCollection personCollection);

    void deleteById(String id);

}

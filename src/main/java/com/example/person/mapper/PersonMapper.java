package com.example.person.mapper;

import com.example.person.collection.PersonCollection;
import com.example.person.dto.Person;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PersonMapper extends CollectionMapper<PersonCollection, Person> {
}

package com.example.person.mapper;

public interface CollectionMapper<C, D> {

    C dtoToCollection(D dto);

    D collectionToDto(C collection);
}

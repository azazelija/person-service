package com.example.person.service;

import com.example.person.collection.PersonCollection;
import com.example.person.dto.Person;
import com.example.person.mapper.PersonMapperImpl;
import com.example.person.repository.PersonRepository;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import java.math.BigInteger;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PersonServiceTest {

    @Mock
    PersonRepository personRepository;

    @Spy
    PersonMapperImpl personMapper;

    @InjectMocks
    PersonService personService;

    Person personDto = new Person();
    PersonCollection personCollection = new PersonCollection();

    @BeforeEach
    void setUp() {
        ObjectId id = new ObjectId();
        String name = "Kristina";
        String email = "kris@mail.ru";
        String phone = "8-800-555-35-35";

        personDto.setId(id);
        personDto.setName(name);
        personDto.setPhone(phone);
        personDto.setEmail(email);

        BeanUtils.copyProperties(personDto, personCollection);
    }

    @Test
    void getById() {
        when(personRepository.findById(anyString())).thenReturn(Optional.ofNullable(personCollection));

        Person returnedPerson = personService.getById(anyString());
        assertThat(returnedPerson)
                .usingRecursiveComparison()
                .isEqualTo(personDto);
    }

    @Test
    void create() {
        when(personRepository.save(any())).thenReturn(personCollection);

        personDto.setId(null);
        Person returnedPerson = personService.create(personDto);
        assertThat(returnedPerson)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(personDto);

        assertThat(returnedPerson.getId())
                .isEqualTo(personCollection.getId());
    }

    @Test
    void update() {
        when(personRepository.save(any())).thenReturn(personCollection);

        Person returnedPerson = personService.create(personDto);
        assertThat(returnedPerson)
                .usingRecursiveComparison()
                .isEqualTo(personDto);
    }

    @Test
    void deleteById() {
        personService.deleteById(any());
    }
}
package com.example.person.controller;

import com.example.person.collection.PersonCollection;
import com.example.person.dto.Person;
import com.example.person.mapper.PersonMapper;
import com.example.person.repository.PersonRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@ItTestWithMockMvc
class PersonControllerTest {

    @MockBean
    private PersonRepository personRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    ObjectMapper jacksonObjectMapper;

    Person personDto;
    PersonCollection personCollection;

    @BeforeEach
    void setUp() {
        personDto = new Person();
        personCollection = new PersonCollection();

        ObjectId id = new ObjectId();
        String name = "Kristina";
        String email = "kris@mail.ru";
        String phone = "8-800-555-35-35";

        personDto.setId(id);
        personDto.setName(name);
        personDto.setPhone(phone);
        personDto.setEmail(email);

        personCollection = personMapper.dtoToCollection(personDto);
    }

    @Test
    void getById() throws Exception {
        given(personRepository.findById(personCollection.getId().toString())).willReturn(Optional.of(personCollection));

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/person/{id}", personCollection.getId())
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder)
                .andReturn();

        assertThat(mvcResult.getResponse().getStatus())
                .isEqualTo(200);

        Person response = jacksonObjectMapper.readValue(mvcResult.getResponse().getContentAsString(), Person.class);
        assertThat(response)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(personCollection);

    }

    @Test
    void create() throws Exception {
        personCollection.setId(null);

        given(personRepository.save(any(PersonCollection.class))).willReturn(personCollection);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jacksonObjectMapper.writeValueAsBytes(personCollection))
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder)
                .andReturn();

        assertThat(mvcResult.getResponse().getStatus())
                .isEqualTo(200);

        Person response = jacksonObjectMapper.readValue(mvcResult.getResponse().getContentAsString(), Person.class);
        assertThat(response)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(personCollection);
    }

    @Test
    void update() throws Exception {
        String req = "{\n" +
                "    \"id\": \"629d005d6ef9223b80d8592e\",\n" +
                "    \"name\": \"Masha\",\n" +
                "    \"email\": \"email@yandex.ru\",\n" +
                "    \"phone\": \"77007007777\"\n" +
                "}";
        given(personRepository.findById(any(ObjectId.class))).willReturn(Optional.of(personCollection));
        given(personRepository.save(any(PersonCollection.class))).willReturn(personCollection);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(req)
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder)
                .andReturn();

        assertThat(mvcResult.getResponse().getStatus())
                .isEqualTo(200);
    }

    @Test
    void deleteById() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/person/{id}", personCollection.getId())
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder)
                .andReturn();

        assertThat(mvcResult.getResponse().getStatus())
                .isEqualTo(200);
    }
}
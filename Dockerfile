FROM openjdk:18
ADD build/libs/person-service-1.0.0.jar person-service-1.0.0.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/person-service-1.0.0.jar"]